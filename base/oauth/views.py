import imp
import random
import string
from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.decorators import api_view
from base.oauth.serializers import UserSerializer
from rest_framework import status
from base.models import User
from datetime import datetime
import json

@api_view(['GET'])
def getRoutes(request):
    routes = [
        '/oauth/token',
        '/oauth/resource',
    ]

    return Response(routes)

@api_view(['GET','POST'])
def token(request):
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        data_input = serializer.data
        try:
            user = User.objects.get(username=data_input["username"])
        except User.DoesNotExist:
            user = None

        if user != None:
            if user.password == data_input["password"]:
                if user.client_id == data_input["client_id"]:
                    #if data_input["grant_type"] == "password":
                        if user.client_secret == data_input["client_secret"]:
                            user.token = get_token()
                            if user.refresh_token == None:
                                user.refresh_token = get_token()
                            user.token_created = datetime.now()
                            user.save()
                            output = {
                                "access_token" : user.token,
                                "expires_in" : 300,
                                "token_type" : "Bearer",
                                "scope" : None,
                                "refresh_token" : user.refresh_token,
                            }
                            return Response(output, status=status.HTTP_200_OK)
        #serializer.save()
        output = {
            "error":"invalid_request",
            "Error_description":"ada kesalahan masbro!"
        }
        return Response(output, status=status.HTTP_401_UNAUTHORIZED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def resource(request):
    token_request = request.META.get('HTTP_AUTHORIZATION')

    try:
        user = User.objects.get(token=token_request)
    except User.DoesNotExist:
        user = None
    
    if user != None:
        d1 = int(datetime.now().strftime("%Y%m%d%H%M%S"))
        d2 = int(user.token_created.strftime("%Y%m%d%H%M%S"))
        #d2 = strptime(user.token_created, "%Y%m%d%H%M%S")
        selisih = (d1 - d2)

        if 0  <= selisih  <= 300:
            output = {
                        "access_token" : user.token,
                        "client_id" : user.client_id,
                        "user_id" : user.username,
                        "full_name" : user.fullname,
                        "npm" : user.npm,
                        "expires" : None,
                        "refresh_token" : user.refresh_token
                    }

            return Response(output, status=status.HTTP_200_OK)
    
    output = {
            "error" : "invalid_token",
	        "error_description" : "Token Salah masbro"
        }
    return Response(output, status=status.HTTP_401_UNAUTHORIZED)

def get_token():
    token = ''.join(random.choice(string.ascii_lowercase) for x in range(40))
    return token
