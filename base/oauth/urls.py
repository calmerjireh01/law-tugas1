from django.urls import path
from. import views

urlpatterns = [
    path('', views.getRoutes),
    path('token/', views.token),
    path('resource/', views.resource)
]